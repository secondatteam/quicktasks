Различные задачки
===

## Проект команды №2 с различными заданиями (дз, дополнительные задачки и т.п.)

### Совместные задачки
- [28.09 - Лампочки, Ближайшее число](https://gitlab.com/secondatteam/quicktasks/tree/hw2809_simple_algorithms)

### Домашние задания (по алфавиту)

#### [Aleksandr Zhuravel](https://gitlab.com/AleksandrZhuravel)
- [29.09 - HTTP-запросы](https://gitlab.com/secondatteam/quicktasks/tree/zhuravel_http)
- [30.09 - Rest Assured](https://gitlab.com/secondatteam/quicktasks/tree/zhuravel_rest-assured) 
- [01.10 - Зависимые запросы](https://gitlab.com/secondatteam/quicktasks/tree/zhuravel_cookies)

#### [Dmitriy Kusakin](https://gitlab.com/DaemonKus)
- [29.09 - HTTP-запросы](https://gitlab.com/secondatteam/quicktasks/tree/kusakin_http)
- [30.09 - Rest Assured + 01.10 - Зависимые запросы](https://gitlab.com/secondatteam/quicktasks/tree/kusakin_rest-assured)

#### [Ivan Gyulumyan](https://gitlab.com/ivan_gyulumyan)
- [29.09 - HTTP-запросы](https://gitlab.com/secondatteam/quicktasks/tree/gyulumyan_http)
- [30.09 - Rest Assured](https://gitlab.com/secondatteam/quicktasks/tree/gyulumyan_rest-assured)

#### [Ulukbek Toychuev](https://gitlab.com/Ulukbek-Toychuev)
- [30.09 - Rest Assured](https://gitlab.com/secondatteam/quicktasks/tree/Uluk_rest-assured)



